
public class MethodeBeispiel {

	public static void main(String[] args) {
		
		//sayHello("Max");		
		//sayHello();		
		// add(7,10);
		
		//addDouble(3.5,7.7);
		
		summe(8,6,7);  //Ausgabe:  8 + 6 + 7 = 16
		public static void test() {
	}
	
	public static void test() {
		
	}
	
	public static void summe(int z1, int z2, int z3) {
		int erg = z1 + z2 + z3;
		
		System.out.println(z1 + "+" + z2 + "+" + z3 + " = " + erg);
	}
	
	public static void addDouble(double zahl1, double zahl2) {
		double erg = zahl1 + zahl2;
		
		System.out.println("Zahl1: " + zahl1);
		System.out.println("Zahl2: " + zahl2);
		System.out.println("Ergebnis: " + erg);
	}
	
	public static void add(int zahl1, int zahl2 ) {
		int erg = zahl1 + zahl2;
		System.out.println(zahl1 + "+"+zahl2 + " = "+ erg);
	}
	
	public static void sayHello() {
		System.out.println("hello ...");		
	}
	
	public static void sayHello(String name) {
		System.out.println("hello "+name);		
	}

}
